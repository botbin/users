package repo

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/botbin/users/pkg/data"
	"gitlab.com/botbin/users/pkg/data/entity"
)

// A UserRepository grants access to a collection of User entities.
type UserRepository interface {
	Save(*entity.User) (*entity.User, error)
	FindAll() ([]entity.User, error)
	Exists(username string) bool
}

// Users is a repository for the User entity.
type Users struct {
	db *sqlx.DB
}

// NewUserRepo configures a production-ready Users repository.
func NewUserRepo() UserRepository {
	return &Users{db: data.GetPostgresConnection()}
}

// Save inserts user into the repository and returns the repository
// represention. user must at least have a value for the ID field.
func (u *Users) Save(user *entity.User) (*entity.User, error) {
	saved := &entity.User{}
	err := u.db.Get(saved, `
		INSERT INTO users.users (id, username)
		VALUES ($1, $2)
		RETURNING *
	`, user.ID, user.Username)
	return saved, err
}

// FindAll gets all users from the repository.
func (u *Users) FindAll() ([]entity.User, error) {
	users := make([]entity.User, 0)
	err := u.db.Select(&users, "SELECT * FROM users.users")
	return users, err
}

// Exists determines if a user exists in the repository.
func (u *Users) Exists(username string) bool {
	exists := false
	err := u.db.QueryRow(`
		SELECT EXISTS (
			SELECT 1 FROM users.users
			WHERE username = $1
			LIMIT 1
		)
	`, username).Scan(&exists)

	return exists && err == nil
}
