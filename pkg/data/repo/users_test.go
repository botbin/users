package repo_test

import (
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/satori/go.uuid"

	"github.com/jmoiron/sqlx"
	"gitlab.com/botbin/users/pkg/data"
	"gitlab.com/botbin/users/pkg/data/entity"
	"gitlab.com/botbin/users/pkg/data/repo"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// UsersTestSuite tests the Users repository.
type UsersTestSuite struct {
	suite.Suite
	db       *sqlx.DB
	userRepo repo.UserRepository
}

func (us *UsersTestSuite) SetupSuite() {
	us.db = data.GetPostgresConnection()
	us.userRepo = repo.NewUserRepo()
}

func (us *UsersTestSuite) SetupTest() {
	us.db.Exec("DELETE FROM users.users")
}

func (us *UsersTestSuite) TearDownTest() {
	us.db.Exec("DELETE FROM users.users")
}

func (us *UsersTestSuite) TestFindAll() {
	_, err := us.userRepo.Save(&entity.User{ID: int(rand.Int31())})
	assert.Nil(us.T(), err)

	users, err := us.userRepo.FindAll()
	assert.Nil(us.T(), err)
	assert.NotEmpty(us.T(), users)
}

func (us *UsersTestSuite) TestExists() {
	user := uuid.NewV4().String()
	exists := us.userRepo.Exists(user)
	assert.False(us.T(), exists)

	_, err := us.userRepo.Save(&entity.User{
		ID:       int(rand.Int31()),
		Username: user,
	})
	assert.Nil(us.T(), err)

	exists = us.userRepo.Exists(user)
	assert.True(us.T(), exists)
}

func TestUsers(t *testing.T) {
	if os.Getenv("TEST_MODE") == "all" {
		rand.Seed(time.Now().Unix())
		suite.Run(t, new(UsersTestSuite))
	}
}
