package data

import (
	"os"
	"sync"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

var (
	postgresConn     *sqlx.DB
	postgresConnOnce sync.Once
)

// GetPostgresConnection retrieves a cached connection to the postgres database.
func GetPostgresConnection() *sqlx.DB {
	postgresConnOnce.Do(func() {
		conn, err := sqlx.Connect("postgres", os.Getenv("POSTGRES_URL"))
		if err != nil {
			panic(err)
		}

		postgresConn = conn
		tryApplySchema()
	})
	return postgresConn
}

func tryApplySchema() {
	if postgresConn == nil {
		return
	}
	const path = "/var/lib/users/postgres.sql"

	_, err := sqlx.LoadFile(postgresConn, path)
	if err != nil {
		zap.S().Warnw("failed to execute schema file",
			"path", path,
			"error", err,
		)
	} else {
		zap.S().Infow("applied schema file", "path", path)
	}
}
