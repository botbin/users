package entity

import (
	"time"
)

// User models the "users" table of the database.
type User struct {
	ID        int       `db:"id"`
	Username  string    `db:"username"`
	CreatedAt time.Time `db:"created_at"`
}
