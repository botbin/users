package signup_test

import (
	"gitlab.com/botbin/users/pkg/service/signup"
	"net/http/httptest"
	"testing"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/users/pkg/data/entity"
	"gitlab.com/botbin/users/pkg/data/repo/mocks"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

// Tests UserCreator
type UserCreatorTestSuite struct {
	suite.Suite
}

// Ensures that Create stores the username from the authorization server
// response and not just the username it was given. This is important
// because if Create fails after the auth server creates the principal,
// we need to ensure that calling it again will use the username that was
// previously requested.
//
// To illustrate, imagine the following process:
//		1. Create is called with the username 'sam'.
//		2. The auth server creates the identity entry and returns a token.
//		3. Create attempts to store the user but fails. Maybe the db is down.
//		4. Create is called again by the same person -- but with the username 'tom'.
//		5. The auth server, realizing the code belongs to the same person,
//         skips identity creation and returns a token for 'sam'.
//		6. Create now has a token with the username 'sam', but it was given
//		   the username 'tom'.
func (us *UserCreatorTestSuite) TestCreate_Retry() {
	responseUsername := "sam"
	providedUsername := "tom"

	router := gin.New()
	router.POST("/v1/principals", func(ctx *gin.Context) {
		ctx.JSON(201, gin.H{
			"refreshToken": "test",
			"accessToken":  us.createJWT(responseUsername),
			"expiresAt":    1630235147,
		})
	})
	server := httptest.NewServer(router)

	users := new(mocks.UserRepository)
	users.On("Exists", mock.Anything).Return(false)
	users.On("Save", mock.Anything).Return(nil, nil).Run(func(args mock.Arguments) {
		user := args[0].(*entity.User)
		us.Equal(responseUsername, user.Username)
	})

	creator := signup.NewUserCreator(users, server.URL)
	us.NotEqual(responseUsername, providedUsername)
	_, err := creator.Create("test", providedUsername)
	us.Nil(err)

	users.AssertExpectations(us.T())
	server.Close()
}

func (us *UserCreatorTestSuite) createJWT(username string) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub":      1,
		"username": username,
	})
	signed, err := token.SignedString([]byte("test"))
	us.Nil(err)
	return signed
}

func TestUserCreator(t *testing.T) {
	suite.Run(t, new(UserCreatorTestSuite))
}
