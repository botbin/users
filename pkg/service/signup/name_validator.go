package signup

import (
	"errors"
	"regexp"

	"gitlab.com/botbin/users/pkg/data/repo"
)

var (
	// ErrBadName indicates that a name is invalid.
	ErrBadName = errors.New("usernames is invalid")
	// ErrUsernameTaken occurs if a duplicate username is provided when signing up.
	ErrUsernameTaken = errors.New("username is already in use")
)

// nameValidator validates usernames.
type nameValidator struct {
	users     repo.UserRepository
	validName *regexp.Regexp
}

func newNameValidator(users repo.UserRepository) *nameValidator {
	return &nameValidator{
		users:     users,
		validName: regexp.MustCompile(`^[a-z0-9](-?[a-z0-9])*$`),
	}
}

// Check determines if a username can be used.
func (nv *nameValidator) Check(username string) error {
	// TODO: Don't allow offensive names.
	if len(username) > 20 || !nv.validName.MatchString(username) {
		return ErrBadName
	} else if nv.users.Exists(username) {
		// TODO: We'll want to make this check faster.
		// This could be done by checking a bloom filter
		// and then defaulting to the DB if the answer is
		// maybe.
		return ErrUsernameTaken
	}
	return nil
}
