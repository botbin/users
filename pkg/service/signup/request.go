package signup

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// newPrincipalRequest models an HTTP request to create a new principal.
type newPrincipalRequest struct {
	client   *http.Client
	url      string
	code     string
	username string
}

func newNPR(client *http.Client, url, code, username string) *newPrincipalRequest {
	return &newPrincipalRequest{
		client:   client,
		url:      url,
		code:     code,
		username: username,
	}
}

// Send requests a new principal from the authorization server.
// It returns the body of the HTTP response.
func (npr *newPrincipalRequest) Send() (*Response, error) {
	req, err := npr.buildRequest()
	if err != nil {
		return nil, err
	}
	return npr.sendRequest(req)
}

func (npr *newPrincipalRequest) buildRequest() (*http.Request, error) {
	req, err := http.NewRequest("POST", npr.url, nil)
	if err != nil {
		return nil, err
	}

	vals := req.URL.Query()
	vals.Set("code", npr.code)
	vals.Set("username", npr.username)
	req.URL.RawQuery = vals.Encode()

	return req, nil
}

func (npr *newPrincipalRequest) sendRequest(req *http.Request) (*Response, error) {
	res, err := npr.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode != 201 {
		body, _ := ioutil.ReadAll(res.Body)
		if body == nil {
			body = []byte("")
		}
		return nil, fmt.Errorf("expected status code 201; got %d with body %s", res.StatusCode, string(body))
	}

	body := new(Response)
	err = json.NewDecoder(res.Body).Decode(body)
	return body, err
}

// Response models the HTTP response body for a successful signup.
type Response struct {
	RefreshToken string `json:"refreshToken,omitempty"`
	AccessToken  string `json:"accessToken"`
	ExpiresAt    int    `json:"expiresAt"`
}
