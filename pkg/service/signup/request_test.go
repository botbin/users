package signup

import (
	"strings"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/gin-gonic/gin"

	"github.com/stretchr/testify/suite"
)

// Tests newPrincipalRequest
type NewPrincipalRequestTestSuite struct {
	suite.Suite
	client             *http.Client
	authServer         *httptest.Server
	authServerStatus   int
	authServerDelay    time.Duration
	authServerURL      string
	authServerResponse *Response
}

func (ns *NewPrincipalRequestTestSuite) SetupTest() {
	ns.client = &http.Client{Timeout: time.Second}

	authServerPath := "/principals"
	ns.authServerStatus = 201
	ns.authServerResponse = &Response{
		RefreshToken: "refresh",
		AccessToken:  "access",
		ExpiresAt:    1630235147,
	}

	router := gin.New()
	router.POST(authServerPath, func(ctx *gin.Context) {
		time.Sleep(ns.authServerDelay)
		if ns.authServerStatus == 201 {
			ns.Equal("code", ctx.Query("code"))
			ns.Equal("username", ctx.Query("username"))

			ctx.JSON(201, ns.authServerResponse)
		} else {
			ctx.AbortWithStatusJSON(400, gin.H{"error": "test"})
		}
	})
	ns.authServer = httptest.NewServer(router)
	ns.authServerURL = ns.authServer.URL + authServerPath
}

func (ns *NewPrincipalRequestTestSuite) TearDownTest() {
	ns.authServer.Close()
	ns.authServerStatus = 201
	ns.authServerDelay = 0
}

func (ns *NewPrincipalRequestTestSuite) TestSend_Success() {
	npr := newNPR(ns.client, ns.authServerURL, "code", "username")
	res, err := npr.Send()
	ns.Nil(err)
	ns.EqualValues(ns.authServerResponse, res)
}

func (ns *NewPrincipalRequestTestSuite) TestSend_404() {
	npr := newNPR(ns.client, ns.authServerURL, "code", "username")
	ns.authServerStatus = 400
	_, err := npr.Send()
	ns.NotNil(err)
}

func (ns *NewPrincipalRequestTestSuite) TestSend_Timeout() {
	npr := newNPR(ns.client, ns.authServerURL, "code", "username")
	ns.authServerDelay = ns.client.Timeout * 2
	_, err := npr.Send()
	ns.NotNil(err)
}

func (ns *NewPrincipalRequestTestSuite) TestSend_InvalidURL() {
	parts := strings.Split(ns.authServerURL, "/")
	path := parts[len(parts)-1]
	npr := newNPR(ns.client, path, "code", "username")

	_, err := npr.Send()
	ns.NotNil(err)
}

func TestNewPrincipalRequest(t *testing.T) {
	suite.Run(t, new(NewPrincipalRequestTestSuite))
}
