package signup

import (
	"testing"

	"github.com/dgrijalva/jwt-go"

	"github.com/stretchr/testify/suite"
)

// Tests principalResponseParser
type PrincipalResponseParserTestSuite struct {
	suite.Suite
}

// Ensure that the claims of tokens retrieved from the authorization server
// can be read.
func (ps *PrincipalResponseParserTestSuite) TestParse_Success() {
	principal, username := 1, "tester"
	body := &Response{
		RefreshToken: "test",
		AccessToken:  ps.createJWT(principal, username),
		ExpiresAt:    1630235147,
	}

	parser := newResponseParser()
	lens, err := parser.Parse(body)
	ps.Nil(err)
	ps.Equal(principal, lens.Principal)
	ps.Equal(username, lens.Username)
}

func (ps *PrincipalResponseParserTestSuite) createJWT(principal int, username string) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub":      principal,
		"username": username,
	})
	signed, err := token.SignedString([]byte("test"))
	ps.Nil(err)
	return signed
}

// TODO: Test cases where the claims are missing or the wrong data type.

func TestPrincipalResponseParser(t *testing.T) {
	suite.Run(t, new(PrincipalResponseParserTestSuite))
}
