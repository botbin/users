package signup

import (
	"errors"
	"net/http"
	"strings"
	"time"

	"gitlab.com/botbin/users/pkg/data/entity"
	"gitlab.com/botbin/users/pkg/data/repo"
	"go.uber.org/zap"
)

var (
	// ErrDuplicateSignup occurs if a user attempts to sign up twice.
	ErrDuplicateSignup = errors.New("an account has already been created")
)

// UserCreator handles creation of new user accounts.
type UserCreator struct {
	principalURL string
	parser       *principalResponseParser
	validator    *nameValidator
	client       *http.Client
	users        repo.UserRepository
}

// NewUserCreator configures a new UserCreator.
//
// authAddr is the URL for the authorization server. E.g., http://auth:8000
func NewUserCreator(users repo.UserRepository, authAddr string) *UserCreator {
	return &UserCreator{
		principalURL: authAddr + "/v1/principals",
		parser:       newResponseParser(),
		validator:    newNameValidator(users),
		client:       &http.Client{Timeout: time.Second * 3},
		users:        users,
	}
}

// Create makes an account for the person that provided the given code.
// It will return a token set from the authorization server or fail if
// the code provider already had an account.
func (uc *UserCreator) Create(code, username string) (*Response, error) {
	username = strings.TrimSpace(username)
	if err := uc.validator.Check(username); err != nil {
		return nil, err
	}

	req := newNPR(uc.client, uc.principalURL, code, username)
	res, err := req.Send()
	if err != nil {
		return nil, err
	}

	err = uc.store(res)
	return res, err
}

func (uc *UserCreator) store(res *Response) error {
	lens, err := uc.parser.Parse(res)
	if err != nil {
		zap.S().Errorw("could not parse token", "error", err, "token_response", res)
		return err
	}

	user := &entity.User{ID: lens.Principal, Username: lens.Username}
	_, err = uc.users.Save(user)

	if err != nil {
		zap.S().Infow("error saving user", "error", err, "subject", lens)
		// This is the best guess.
		return ErrDuplicateSignup
	}
	return nil
}
