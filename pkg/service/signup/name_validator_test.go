package signup

import (
	"testing"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/users/pkg/data/repo/mocks"
)

// Tests nameValidator
type NameValidatorTestSuite struct {
	suite.Suite
}

// Ensure that usernames that do not fit the defined regex are marked as
// invalid.
func (ns *NameValidatorTestSuite) TestCheck_Regex() {
	tests := []regexTestCase{
		{"test", true},
		{"test-test", true},
		{"test_test", false},
		{"test/test", false},
		{"test.test", false},
		{"test test", false},
		{"", false},
		{" ", false},
		{"0123456789", true},
		{"012345678901234567890", false},
		{"t", true},
		{"mIxEd0", false},
		{"mixed0", true},
	}

	ns.runRegexTests(tests)
}

type regexTestCase struct {
	Name    string
	IsValid bool
}

func (ns *NameValidatorTestSuite) runRegexTests(tests []regexTestCase) {
	for _, test := range tests {
		users := new(mocks.UserRepository)
		if test.IsValid {
			users.On("Exists", mock.Anything).Return(false)
		}

		validator := newNameValidator(users)
		err := validator.Check(test.Name)

		if test.IsValid {
			ns.Nil(err)
			users.AssertExpectations(ns.T())
		} else {
			ns.Equal(ErrBadName, err)
			// The Exists method shouldn't be called if the format isn't right.
		}
	}
}

// Ensure that duplicate names are marked as invalid.
func (ns *NameValidatorTestSuite) TestCheck_Duplicate() {
	users := new(mocks.UserRepository)
	users.On("Exists", mock.Anything).Return(true)

	validator := newNameValidator(users)

	err := validator.Check("test")
	ns.Equal(ErrUsernameTaken, err)

	users.AssertExpectations(ns.T())
}

func TestNameValidator(t *testing.T) {
	suite.Run(t, new(NameValidatorTestSuite))
}
