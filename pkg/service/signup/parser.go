package signup

import (
	"encoding/json"
	"errors"

	"github.com/dgrijalva/jwt-go"
)

// principalResponseParser parses the response format returned
// when creating a new principal.
type principalResponseParser struct {
	jwtParser *jwt.Parser
}

func newResponseParser() *principalResponseParser {
	return &principalResponseParser{
		jwtParser: &jwt.Parser{
			UseJSONNumber: true,
		},
	}
}

// Parse retrieves relevant claims from the access token in body.
func (prr *principalResponseParser) Parse(body *Response) (tokenLens, error) {
	claims, err := prr.getClaims(body)
	if err != nil {
		return tokenLens{}, err
	}
	return prr.buildLens(claims)
}

func (prr *principalResponseParser) getClaims(body *Response) (jwt.MapClaims, error) {
	token, _, err := prr.jwtParser.ParseUnverified(body.AccessToken, jwt.MapClaims{})
	if err != nil {
		return nil, err
	}
	return token.Claims.(jwt.MapClaims), nil
}

func (prr *principalResponseParser) buildLens(claims jwt.MapClaims) (tokenLens, error) {
	usernameClaim, exists := claims["username"]
	if !exists {
		return tokenLens{}, errors.New("missing 'username' claim")
	}
	username, ok := usernameClaim.(string)
	if !ok {
		return tokenLens{}, errors.New("expected 'username' claim to be a string")
	}

	subClaim, exists := claims["sub"]
	if !exists {
		return tokenLens{}, errors.New("missing 'sub' claim")
	}
	subNum, ok := subClaim.(json.Number)
	if !ok {
		return tokenLens{}, errors.New("expected 'sub' claim to be an int")
	}
	sub, err := subNum.Int64()
	if err != nil {
		return tokenLens{}, err
	}

	return tokenLens{
		Principal: int(sub),
		Username:  username,
	}, nil
}

type tokenLens struct {
	Principal int
	Username  string
}
