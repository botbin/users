package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// Server is the root HTTP server for this service.
// It handles mapping routes to the various controllers.
type Server struct {
	controllers []Controller
}

// Start opens the HTTP server up to requests.
func (s *Server) Start(port string) error {
	router := gin.Default()
	for _, ctrl := range s.controllers {
		ctrl.Accept(router)
		zap.S().Infof("using controller %T", ctrl)
	}

	return http.ListenAndServe(":"+port, router)
}

// NewServer configures a production-ready Server instance.
func NewServer() *Server {
	return &Server{
		controllers: []Controller{
			NewUsersController(),
		},
	}
}
