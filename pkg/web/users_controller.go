package web

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/users/pkg/data/repo"
	"gitlab.com/botbin/users/pkg/service/signup"
	"go.uber.org/zap"
)

// UsersController handles the /users resource.
type UsersController struct {
	creator *signup.UserCreator
}

// NewUsersController configures a UsersController.
// This method requires the AUTH_ADDR variable to be present in the environment.
func NewUsersController() *UsersController {
	url := os.Getenv("AUTH_ADDR")
	if url == "" {
		zap.S().Panicw("missing environment var AUTH_ADDR")
	}
	users := repo.NewUserRepo()

	return &UsersController{
		creator: signup.NewUserCreator(users, url),
	}
}

// Accept adds the route handlers to router.
func (us *UsersController) Accept(router *gin.Engine) {
	router.POST("/v1/users", us.createUser)
}

func (us *UsersController) createUser(ctx *gin.Context) {
	code := ctx.Query("code")
	if code == "" {
		ctx.JSON(400, gin.H{"message": "expected 'code' query param"})
		return
	}

	username := ctx.Query("username")
	if username == "" {
		ctx.JSON(400, gin.H{"message": "expected 'username' query param"})
		return
	}

	res, err := us.creator.Create(code, username)
	if err != nil {
		ctx.JSON(400, gin.H{"message": us.userify(err)})
	} else {
		ctx.JSON(201, res)
	}
}

func (us *UsersController) userify(err error) string {
	zap.S().Infow("user creation failed", "error", err)

	var msg string
	if err == signup.ErrBadName || err == signup.ErrDuplicateSignup || err == signup.ErrUsernameTaken {
		msg = err.Error()
	} else {
		msg = "failed to create user"
	}
	return msg
}
