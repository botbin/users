package web

import (
	"github.com/gin-gonic/gin"
)

// Controller types connect HTTP requests with application logic.
type Controller interface {
	Accept(r *gin.Engine)
}