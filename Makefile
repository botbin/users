PROJECT_NAME := users
COMPOSE_TEST_FILE := docker-compose.test.yaml

# Variables for integration with the API gateway
GATEWAY_NAME := users
GATEWAY_S3_BUCKET := $(if $(GATEWAY_S3_BUCKET),$(GATEWAY_S3_BUCKET),s3://config.gateway.dev.botbin.io)
GATEWAY_S3_REGION := $(if $(GATEWAY_S3_REGION),$(GATEWAY_S3_REGION),us-east-2)

build:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ${PROJECT_NAME} cmd/main.go

# Integration tests; no external deps required
test:
	go test -coverprofile=cov.out ./pkg/...

test_all:
	docker-compose -f docker/${COMPOSE_TEST_FILE} down -v || true
	docker-compose -f docker/${COMPOSE_TEST_FILE} up --build --abort-on-container-exit
	docker-compose -f docker/${COMPOSE_TEST_FILE} down -v

# Uploads the API definition to the s3 bucket for the API gateway
gateway:
	aws s3 cp ./schema/gateway.toml ${GATEWAY_S3_BUCKET}/apis/${GATEWAY_NAME}/gateway.toml --region ${GATEWAY_S3_REGION}

version:
	$(eval VERSION=$(shell ./version.sh))

image: version
	docker build -f docker/Dockerfile -t ${REGISTRY}/${PROJECT_NAME}:${VERSION} .

deploy_image: image
	docker push ${REGISTRY}/${PROJECT_NAME}:${VERSION}