package main

import (
	"gitlab.com/botbin/users/pkg/web"
	"go.uber.org/zap"
)

func init() {
	l, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	zap.ReplaceGlobals(l)
}

const (
	app     = "users"
	version = "0.6.4"
	port    = "8081"
)

func main() {
	zap.S().Infof("started %s v%s on port %s", app, version, port)

	if err := web.NewServer().Start(port); err != nil {
		zap.S().Errorw("server error", "error", err)
	}
}
