/**
 * This schema was designed for PostgreSQL.
 *
 * The schema effectively belongs to this service, so changes
 * can be made as long as this service continues to fulfill
 * its API contracts.
 */

CREATE SCHEMA users;

CREATE TABLE users.users (
    id integer PRIMARY KEY,
    username text UNIQUE NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW()
);