# users
This service manages user accounts.

## Tests
Testing is done with the `make test` and `make test_all` commands.
The test recipe will run only the subset of tests that do not require
external dependencies. test_all executes the full suite, but it requires
a Docker Compose installation.

## Environment Variables
- POSTGRES_URL - A connection string for PostgreSQL
- AUTH_ADDR - The address for the authorization server
